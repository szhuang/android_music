package com.example.lesson.activitys;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.lesson.R;
import com.example.lesson.service.PlayerService;
import com.example.lesson.tools.AppConstant;
import com.example.lesson.tools.MediaUtil;
import com.example.lesson.tools.Mp3Info;

import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.Iterator;
        import java.util.List;
        import java.util.Map;

public class MainActivity extends Activity {

    private boolean isPlaying;
    private int musicId;
    private ListView lv;
    private TextView tv;
    List<Mp3Info> mp3Infos;
    private BroadcastReceiver receiver;
    private int current_time;
    private int sum_time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findView();
        setListAdapter(mp3Infos);
        registerReceiver();
        setListener();
    }

    private void setListener() {

        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                // TODO Auto-generated method stub
                ListView listview = (ListView) parent;
                Map<String,Object> map = (Map<String, Object>) listview.getItemAtPosition(position);
                tv.setText("    		          正在播放   “" +(String)map.get("title")+ "”......");
                musicId=position;
                Intent intent = new Intent();
                intent.putExtra("musicId", position);
                intent.putExtra("duration",sum_time);
                intent.putExtra("MSG", AppConstant.PLAY_MSG);
                intent.setClass(MainActivity.this,PlayerService.class);
                startService(intent);		//启动服务

                Intent iB=new Intent();
                iB.setAction(AppConstant.MUSIC_PLAY);
                iB.putExtra("musicId",position);
                sendBroadcast(iB);

                }

        });

        tv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, MusicPlaying.class);
                intent.putExtra("musicId",musicId);
                intent.putExtra("duration",sum_time);
                intent.putExtra("isPlaying",isPlaying);
                startActivity(intent);
            }
        });
    }

    public void findView() {
        lv = (ListView) findViewById(R.id.listView1);
        tv=(TextView)findViewById(R.id.songInfo);
        mp3Infos= MediaUtil.getMp3Infos(MainActivity.this);
    }

    public void setListAdapter(List<Mp3Info> mp3Infos) {
        List<HashMap<String, Object>> mp3list = new ArrayList<HashMap<String, Object>>();
        for (Iterator iterator = mp3Infos.iterator(); iterator.hasNext();) {
            Mp3Info mp3Info = (Mp3Info) iterator.next();
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("image", R.drawable.music_icon);
            map.put("title", mp3Info.getTitle());
            map.put("Artist", mp3Info.getArtist());
            map.put("duration",  MediaUtil.formatTime(mp3Info.getDuration()));
            map.put("size", mp3Info.getSize());
            map.put("url", mp3Info.getUrl());
            mp3list.add(map);
        }
        SimpleAdapter mAdapter = new SimpleAdapter(this, mp3list,
                R.layout.main2_item, new String[] { "image", "title", "Artist","duration" },
                new int[] { R.id.imageView1, R.id.textView1, R.id.textView2, R.id.textView3 });
        lv.setAdapter(mAdapter);
    }


    public void registerReceiver(){
        receiver=new MainActivityReceiver();
        IntentFilter filter=new IntentFilter();
        filter.addAction(AppConstant.MUSIC_CURRENT);
        filter.addAction(AppConstant.MUSIC_DURATION);
        filter.addAction(AppConstant.UPDATE_ACTION);
        filter.addAction(AppConstant.MUSIC_PAUSE);
        MainActivity.this.registerReceiver(receiver,filter);
    }

    public class MainActivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getAction();
            if(action.equals(AppConstant.MUSIC_CURRENT)){
                current_time=intent.getIntExtra("currentPosition",-1);
            }else if(action.equals(AppConstant.MUSIC_DURATION)){
                sum_time=intent.getIntExtra("duration",-1);
            }else if(action.equals(AppConstant.UPDATE_ACTION)){
                musicId=intent.getIntExtra("musicId",-1);
                if(musicId>=0&&musicId<mp3Infos.size()){
                    String title=mp3Infos.get(musicId).getTitle();
                    tv.setText("    		          正在播放   “" +title+ "”......");
                }
            }else if(action.equals(AppConstant.MUSIC_PAUSE)){
                isPlaying=intent.getBooleanExtra("isPlaying",true);
            }
        }
    }



}

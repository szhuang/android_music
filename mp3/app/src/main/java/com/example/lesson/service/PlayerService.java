package com.example.lesson.service;

import java.util.ArrayList;
import java.util.List;

import com.example.lesson.activitys.MusicPlaying;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Handler;
import android.os.IBinder;
import android.view.animation.AnimationUtils;

import com.example.lesson.R;
import com.example.lesson.tools.AppConstant;
import com.example.lesson.tools.MediaUtil;
import com.example.lesson.tools.Mp3Info;

/***
 * 2013/5/25
 * @author wwj
 * 音乐播放服务
 */
@SuppressLint("NewApi")
public class PlayerService extends Service {

	private MediaPlayer mediaPlayer; // 媒体播放器对象
	private int musicId; 			// 音乐文件位置
	private int msg;				//播放信息
	private List<Mp3Info> mp3Infos;	//存放Mp3Info对象的集合
	private int status = 3;			//播放状态，默认为顺序播放
	//private MyReceiver myReceiver;	//自定义广播接收器
	private int currentTime;		//当前播放进度
	private int duration;			//播放长度
    private boolean threadDisable=false;



    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

	@Override
	public void onCreate() {
		super.onCreate();
        this.mediaPlayer=new MediaPlayer();
		mp3Infos = MediaUtil.getMp3Infos(PlayerService.this);

	}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        musicId = intent.getIntExtra("musicId",0);		//歌曲id
        msg = intent.getIntExtra("MSG", 0);			//播放信息
        if(msg== AppConstant.PLAY_MSG){

            musicId = intent.getIntExtra("musicId",0);		//歌曲id
            play(0);
        }else if(msg==AppConstant.PAUSE_MSG){
            pause();
        }else if(msg==AppConstant.CONTINUE_MSG){
            resume();
        }else if(msg==AppConstant.PRIVIOUS_MSG){
            next();
        }else if(msg==AppConstant.NEXT_MSG){
            previous();
        }else if (msg == AppConstant.PROGRESS_CHANGE) {
            currentTime = intent.getIntExtra("progress", -1);

            play(currentTime);

         }




        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!threadDisable) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Intent intent=new Intent();
                    intent.setAction(AppConstant.MUSIC_CURRENT);
                    intent.putExtra("currentPosition",mediaPlayer.getCurrentPosition());
                    sendBroadcast(intent);
                }
            }
        }).start();

        return super.onStartCommand(intent, flags, startId);
    }

	@Override
	public void onDestroy() {
		if (mediaPlayer != null) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
        this.threadDisable=true;
	}


    private Handler handler=new Handler(){

        public void handleMessage(){
             if(mediaPlayer!=null){
                 currentTime=mediaPlayer.getCurrentPosition();
                 Intent intent=new Intent();
                 intent.setAction(AppConstant.MUSIC_CURRENT);
                 intent.putExtra("currentPosition",currentTime);
                 sendBroadcast(intent);
                 handler.sendEmptyMessageDelayed(1,1000);
             }
        }
    };

    /**
     * 播放音乐
     */
    private void play(int currentTime) {
        Intent intent=new Intent();
        intent.setAction(AppConstant.MUSIC_PAUSE);
        intent.putExtra("isPlaying",true);
        sendBroadcast(intent);
        try {
            //initLrc();
//            handler.post(mRunnable);
            mediaPlayer.reset();// 把各项参数恢复到初始状态
            mediaPlayer.setDataSource(mp3Infos.get(musicId).getUrl());
            mediaPlayer.prepare(); // 进行缓冲
            mediaPlayer.setOnPreparedListener(new PreparedListener(currentTime));// 注册一个监听器
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	/*
	 * 实现一个OnPrepareLister接口,当音乐准备好的时候开始播放
	 */
	private final class PreparedListener implements OnPreparedListener {
		private int currentTime;

		public PreparedListener(int currentTime) {
			this.currentTime = currentTime;
		}

		@Override
		public void onPrepared(MediaPlayer mp) {
			mediaPlayer.start(); // 开始播放

            if (currentTime > 0) { // 如果音乐不是从头播放
                mediaPlayer.seekTo(currentTime);
            }

            Intent intent=new Intent();
            intent.setAction(AppConstant.MUSIC_DURATION);
            intent.putExtra("duration",mediaPlayer.getDuration());
            sendBroadcast(intent);

		}
	}

    private void pause(){
        if(mediaPlayer!=null&&mediaPlayer.isPlaying()){
            mediaPlayer.pause();
            Intent intent=new Intent();
            intent.setAction(AppConstant.MUSIC_PAUSE);
            intent.putExtra("isPlaying",false);
            sendBroadcast(intent);
        }
    }

    private void resume(){
        if(!mediaPlayer.isPlaying()){
            mediaPlayer.start();
            Intent intent=new Intent();
            intent.setAction(AppConstant.MUSIC_PAUSE);
            intent.putExtra("isPlaying",true);
            sendBroadcast(intent);
        }
    }

    /**
     * 初始化歌词配置
     */

    public void previous(){
        play(0);
        Intent sendIntent=new Intent(AppConstant.UPDATE_ACTION);
        sendIntent.putExtra("musicId",musicId);
        sendBroadcast(sendIntent);
    }
    public void next(){
        play(0);
        Intent sendIntent=new Intent(AppConstant.UPDATE_ACTION);
        sendIntent.putExtra("musicId",musicId);
        sendBroadcast(sendIntent);
    }
}
